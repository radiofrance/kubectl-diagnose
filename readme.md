# kubectl-diagnose

Doctissimo for kubectl

## install

`go install gitlab.com/radiofrance/kubectl-diagnose`

## Usage

`kubectl diagnose [ --namespace=<namespace> ] RESOURCE`
