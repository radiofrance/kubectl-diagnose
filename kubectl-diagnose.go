package main

import (
	"context"
	"fmt"
	"log"
	"os"

	cli "github.com/jawher/mow.cli"
	"gitlab.com/radiofrance/kubecli"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/reference"
)

type Config struct {
	Resource  string
	Namespace string
}

func main() {
	app := cli.App("kubectl-diagnose", "abc")

	app.Spec = "[ --namespace=<namespace> ] RESOURCE"

	config := Config{}
	app.StringOptPtr(&config.Namespace, "n namespace", "", "a namespace")
	app.StringArgPtr(&config.Resource, "RESOURCE", "", "the name of the resource to diagnose")

	app.Action = func() {
		kcli, err := kubecli.New("")
		if err != nil {
			log.Fatal(err)
		}

		if config.Namespace != "" {
			kcli.Namespace = config.Namespace
		}

		if diagnoseService(kcli, config) {
			fmt.Printf("\n")
		}
		if diagnoseHpa(kcli, config) {
			fmt.Printf("\n")
		}
		if isDeployment, _ := diagnoseDeployment(kcli, config); isDeployment {
			fmt.Printf("\n")
		}
		if isStatefulSet, _ := diagnoseStatefulSet(kcli, config); isStatefulSet {
			fmt.Printf("\n")
		}
	}

	_ = app.Run(os.Args)
}

func diagnoseHpa(kcli *kubecli.KubeCli, config Config) bool {
	hpa, err := kcli.ClientSet.AutoscalingV1().HorizontalPodAutoscalers(kcli.Namespace).Get(
		context.Background(), config.Resource, metav1.GetOptions{})
	if err != nil {
		return false
	}

	fmt.Printf("HPA:\n")
	fmt.Printf("%s %d %d %d %d/%d\n", hpa.ObjectMeta.Name, *hpa.Spec.MinReplicas, hpa.Spec.MaxReplicas, hpa.Status.CurrentReplicas, hpa.Status.CurrentCPUUtilizationPercentage, *hpa.Spec.TargetCPUUtilizationPercentage)

	return true
}

func diagnoseService(kcli *kubecli.KubeCli, config Config) bool {
	service, err := kcli.ClientSet.CoreV1().Services(kcli.Namespace).Get(
		context.Background(), config.Resource, metav1.GetOptions{})
	if err != nil {
		return false
	}

	fmt.Printf("Service:\n")
	fmt.Printf("%s %s %s\n", service.ObjectMeta.Name, service.Spec.Type, service.Spec.ClusterIP)

	return true
}

func diagnoseDeployment(kcli *kubecli.KubeCli, config Config) (bool, error) {
	deployment, err := kcli.GetDeployment(config.Resource)
	if err != nil {
		return false, err
	}

	replicaSet, err := kcli.GetReplicaSet(deployment)
	if err != nil {
		return false, fmt.Errorf("could not get ReplicaSet: %v", err)
	}

	return diagnosePods(kcli, replicaSet.GetName())
}

func diagnoseStatefulSet(kcli *kubecli.KubeCli, config Config) (bool, error) {
	statefulSet, err := kcli.GetStatefulSet(config.Resource)
	if err != nil {
		return false, err
	}

	return diagnosePods(kcli, statefulSet.GetName())
}

func diagnosePods(kcli *kubecli.KubeCli, setName string) (bool, error) {
	pods, err := kcli.ClientSet.CoreV1().Pods(kcli.Namespace).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return false, fmt.Errorf("could not get Pods: %v", err)
	}
	s := scheme.Scheme
	fmt.Printf("Pods:\n")
	for _, pod := range pods.Items {
		for _, ownerRef := range pod.GetOwnerReferences() {
			if (ownerRef.Kind == "ReplicaSet" || ownerRef.Kind == "StatefulSet") && ownerRef.Name == setName {
				isOk := true
				restart := int32(0)
				containerOk := 0
				for _, containerStatus := range pod.Status.ContainerStatuses {
					restart += containerStatus.RestartCount
					if containerStatus.State.Running == nil {
						isOk = false
					} else {
						containerOk++
					}
				}

				if isOk {
					fmt.Printf("%v %v/%v (%v), %v restart\n", pod.Name, containerOk, len(pod.Status.ContainerStatuses), pod.Status.Phase, restart)
				} else {
					ref, err := reference.GetReference(s, pod.DeepCopyObject())
					if err != nil {
						return false, fmt.Errorf("reference errors %v", err)
					}

					events, err := kcli.ClientSet.CoreV1().Events(kcli.Namespace).Search(s, ref)
					if err != nil {
						return false, fmt.Errorf("events errors %v", err)
					}

					fmt.Printf("ERRORS on pod %v\n", pod.Name)
					fmt.Printf("--- events:\n")
					for _, event := range events.Items {
						fmt.Printf("%s %s %s %s\n", event.Type, event.Reason, event.Source.Host, event.Message)
					}

					fmt.Printf("--- pod:\n")
					fmt.Printf("%v %v/%v (%v)\n", pod.Name, containerOk, len(pod.Status.ContainerStatuses), pod.Status.Phase)
					fmt.Printf("--- containers:\n")

					for _, containerStatus := range pod.Status.ContainerStatuses {
						if containerStatus.State.Waiting != nil {
							fmt.Printf("%s %s %s\n", containerStatus.Name, containerStatus.State.Waiting.Reason, containerStatus.State.Waiting.Message)
						} else {
							// TODO
						}
					}

					fmt.Printf("\n\n")
				}
				break
			}
		}
	}
	return true, nil
}
